multibranchPipelineJob("${PROJECT_GROUP}_${PROJECT_NAME}_${DIVISION}_PIPELINE") {
	println("Checking if SSH protocol is passed in GIT_REPOSITORY parameter")
	if ("${GIT_REPOSITORY}".contains('https')) {
		error("https protocol is used. Failing Generator job. Please use SSH protocol for generating new pipeline")
	}
	else {
		println("SSH protocol used. Proceed with pipeline generation")
	}

    println("Generating pipeline: ${PROJECT_GROUP}_${PROJECT_NAME}_PIPELINE")
	// configure Git repository
	branchSources {
		git {
			id 'pipeline'
			remote("${GIT_REPOSITORY}")
			credentialsId("gitlab-ssh")
			includes("${INCLUDE_PATTERN}")
			excludes("${EXCLUDE_PATTERN}")
		}
	}
	
	properties {
            folderLibraries {
                libraries {
                    libraryConfiguration {
                        name 'iafCiPipeline'
                        retriever {
                            modernSCM {
                                scm {
                                    git {
                                        id 'pipeline'
                                        remote 'git@pipeline.git'
                                        credentialsId 'gitlab-ssh'
										traits {
											gitBranchDiscovery()
										}
                                    }
                                }
                            }
                        }
                        allowVersionOverride true
                        defaultVersion 'master'
                        implicit false
                    }
                }
            }
    }
	
	authorization {
		def securityString = SECURITY
		def securityList = securityString.split(',')
		println("List of AD groups / users to configure: " + securityList)
    
		securityList.each { securityListItem ->
      		securityListItemString = securityListItem
			//blocksInheritance(true)
			permission("hudson.model.Item.Build:${securityListItemString}")
			permission("hudson.model.Item.Configure:${securityListItemString}")
			permission("hudson.model.Item.Cancel:${securityListItemString}")
			permission("hudson.model.Item.Delete:${securityListItemString}")
			permission("hudson.model.Item.Discover:${securityListItemString}")
			permission("hudson.model.Item.Read:${securityListItemString}")
			permission("hudson.model.Item.Workspace:${securityListItemString}")
			permission("hudson.model.Run.Delete:${securityListItemString}")
			permission("hudson.model.Run.Replay:${securityListItemString}")
			permission("hudson.model.Run.Update:${securityListItemString}")
		}
    }
	
    orphanedItemStrategy {
        discardOldItems {
          // Sets the number of days to keep old items.
          daysToKeep(3)
          // Sets the number of old items to keep.
          numToKeep(30)
        }
    }
}
